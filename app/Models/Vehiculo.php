<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;

class Vehiculo extends Model
{
    protected $table="vehiculos"; /* Opcional en el caso actual */

    /* Para este modelo vaos utilizar query builder y creamos metodos get y set */

    public function get_infoVehiculo(){
        return $this->infoVehiculo();
    }

    public function get_infoVehiculoPlaca($placa, $tipoVeh){
        return $this->infoVehiculoPlaca($placa, $tipoVeh);
    }

    private function infoVehiculo(){
        return $infoVehiculo= DB::table('vehiculos as veh')
        ->join('propietarios as prop', 'veh.idPropietario', 'prop.idPropietario')
        ->select('veh.idVehiculo', 'veh.tipo', 'veh.marca', 'veh.modelo', 'veh.placa', 
        'prop.idPropietario', 'prop.nombre', 'prop.numIdentificacion')
        ->get();

        return $infoVehiculo;
    }

    private function infoVehiculoPlaca($placa, $tipoVeh){
        return $infoVehiculo= DB::table('vehiculos as veh')
        ->join('propietarios as prop', 'veh.idPropietario', 'prop.idPropietario')
        ->select('veh.idVehiculo', 'veh.tipo', 'veh.marca', 'veh.modelo', 'veh.placa', 
        'prop.idPropietario', 'prop.nombre', 'prop.numIdentificacion')
        ->where('veh.placa', '=', $placa)
        ->where('veh.tipo', '=', $tipoVeh)
        ->get();

        return $infoVehiculo;
    }
}