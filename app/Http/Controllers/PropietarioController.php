<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Propietario;
class PropietarioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function regNewPropietario(Request $request){
        $propietario=new Propietario;

        $propietario->nombre=$request->nombrePropietario;
        $propietario->numIdentificacion=$request->numIdentificacion;
        $propietario->save();

        return response()->json([
            'estado_operacion'=>"Exitosa",
            'data'=>"Propietario Registrado"
        ]);  
    }

    public function listPropietarios(){
        $propietario=new Propietario;
        $listPropietarios=$propietario->all();
        return response()->json([
            'estado_operacion'=>"Exitosa",
            'data'=>$listPropietarios
        ]);  
    }

    public function infoPropietarioID(){
        $propietario=new Propietario;
        $numID=$_GET['numID'];
        $infoPropietario=$propietario->where('numIdentificacion', $numID)->get();
        return response()->json([
            'estado_operacion'=>"Exitosa",
            'data'=>$infoPropietario
        ]);  
        
    }
}
