<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Propietario;
use App\Models\Vehiculo;

class VehiculoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function regNewVehiculo(Request $request){
        $propietario=new Propietario;
        $vehiculo=new Vehiculo;
        try {
            /* Se obtiene informacion de propietario */
            $numID=$request->numIDPropietario;
            $infoPropietario=$propietario->where('numIdentificacion', $numID)->get();
            /* Se verifica que si existe y se procede a asociar en el vehiculo */
            if(count($infoPropietario)>0){
                $vehiculo->tipo=$request->tipoVehiculo;
                $vehiculo->marca=$request->marcaVehiculo;
                $vehiculo->modelo=$request->modeloVehiculo;
                $vehiculo->placa=$request->placaVehiculo;
                $vehiculo->idPropietario=$infoPropietario[0]->idPropietario;
                $vehiculo->save();

                return response()->json([
                    'estado_operacion'=>"Exitosa",
                    'data'=>"Vehiculo Registrado"
                ]); 
            }else{
                return response()->json([
                    'estado_operacion'=>"Fallida",
                    'data'=>"Propietario No Registrado"
                ]); 
            }
        } catch (\Exception $e) {
            return response()->json([
                'estado_operacion'=>"Fallida",
                'data'=>$e->getMessage()
            ]); 
        }   
    }

    public function listVehiculos(){
        $propietario=new Propietario;
        $vehiculo=new Vehiculo;
        $infoVehiculo=$vehiculo->get_infoVehiculo();
        return response()->json([
            'estado_operacion'=>"Exitosa",
            'data'=>$infoVehiculo
        ]); 

    }

    public function infoVehiculoPlaca(){
        $vehiculo=new Vehiculo;
        $placa=$_GET['placa'];
        $tipoVeh=$_GET['tipoVeh'];

        $infoVehiculoPlaca=$vehiculo->get_infoVehiculoPlaca($placa, $tipoVeh);
        
        return response()->json([
            'estado_operacion'=>"Exitosa",
            'data'=>$infoVehiculoPlaca
        ]); 
    }

}